class Parallax {
	static init() {
		if (Parallax.initialized === true) {
			throw new Parallax.MultipleInitializationException();
		}
		Parallax.initialized = true;
		Parallax.id = 1;

		let stylesheet = document.createElement('style');
		Parallax.stylesheet = document.head.appendChild(stylesheet).sheet;

		let containers = Array.from(document.querySelectorAll('.parallax'));
		containers.forEach(container => {
			let image = container.getAttribute('image');
			container.removeAttribute('image');
			container.setAttribute('data-parallax-image', Parallax.id);

			// let imageContainer = document.createElement('div');

			// container.appendChild(imageContainer);

			Parallax.stylesheet.insertRule(`
				[data-parallax-image="${Parallax.id}"] {
					background-image: url(${image});
				}
			`, Parallax.stylesheet.cssRules.length);
			Parallax.id++;
		});
	}

	static MultipleInitializationException(message) {
		return {
			error: "parallax_multiple_initialization",
			message: message || "Attempt to initialize Parallax class more than once."
		};
	}
}

Parallax.init();